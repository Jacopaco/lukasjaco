// This is all you :)
const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');

document.addEventListener('mousedown', start);
document.addEventListener('mouseup', stop);
window.addEventListener('resize', resize);
document.onkeydown = KeyPress;

//global drawing variables
let coord = { x: 0, y: 0 };

var actionBuffer = [];

actionBuffer.push({ action: changeColor, params: 'blue' });

actionBuffer.forEach((action) => {
	action.action(action.params);
});

//keeps track of past actions
var undoBuffer = [];
//keeps track of undone actions until new action is performed
var redoBuffer = [];
//keeps line while it's being draw. On mouse release this gets pushed to drawing history.
var currentLine = [];

//init actions
resize();
initDrawingSettings();

function initDrawingSettings() {
	//set the line style
	ctx.lineWidth = 5;
	//set the brush style
	ctx.lineCap = 'round';
}

function changeColor(newColor) {
	ctx.strokeStyle = newColor;
}

function changeSizeButton(type) {
	newSize = ctx.lineWidth;
	if (type === 'larger') {
		newSize += 5;
	}
	if (type === 'smaller') {
		if (newSize - 5 < 1) newSize = 1;
		else newSize -= 5;
	}
	setSize(newSize);
	document.getElementById('lineSize').value = newSize;
}

function changeSizeInput() {
	newSize = document.getElementById('lineSize').value;
	setSize(newSize);
}

function setSize(newSize) {
	ctx.lineWidth = newSize;
}

function redrawAll() {
	console.log('redraw all');
	clearAll();

	undoBuffer.forEach((line) => {
		ctx.beginPath();
		line.forEach((section) => {
			if (section.mode === 'start') reposition({ clientX: section.x, clientY: section.y });
			draw({ clientX: section.x, clientY: section.y }, false);
		});
	});
}

function clearAll() {
	ctx.clearRect(0, 0, canvas.width, canvas.height);
}

//size the canvas to the size of the containing element -- should be something like parent element?
function resize() {
	ctx.canvas.width = window.innerWidth;
	ctx.canvas.height = window.innerHeight;
}

//the mouse coordinates are relative to the entire screen, so the pixel coordinates need to be mapped to the canvas start location to
//draw on the right spot
function reposition(event) {
	coord.x = event.clientX - canvas.offsetLeft;
	coord.y = event.clientY - canvas.offsetTop;
}

//add event that draws on every mouse movement
function start(event) {
	//just clicking and not moving should also leave a mark.
	//for this we call the draw function a single time, but instead of going from the old location to the new location and drawing a line,
	//we skip the moveTo function and just use the lineTo function, drawing a point from a single location.
	if (event.button != 0) return;
	redoBuffer = [];
	//begin path creates a new distinct line which seperates it from previously drawn lines, making new settings apply to only newly created lines.
	//leaving this out makes it so that if you change the color for a line, all previously drawn lines will change color as well.
	ctx.beginPath();
	draw(event, true);

	currentLine.push({
		x: event.clientX,
		y: event.clientY,
		size: lineSize,
		color: ctx.strokeStyle,
		mode: 'start'
	});

	document.addEventListener('mousemove', mouseMoveEvent);
	// reposition(event);
}

//remove event when button is released
function stop(event) {
	if (event.button != 0) return;
	currentLine[currentLine.length - 1].mode = 'end';
	undoBuffer.push(currentLine);
	currentLine = [];
	document.removeEventListener('mousemove', mouseMoveEvent);
}

function mouseMoveEvent(event) {
	currentLine.push({
		x: event.clientX,
		y: event.clientY,
		size: lineSize,
		color: ctx.strokeStyle,
		mode: 'draw'
	});
	draw(event);
}

//draw the line
function draw(event, singlepoint) {
	//move the line position to the unupdated line coordinate (where it previously was)
	ctx.lineSize = lineSize;
	if (!singlepoint) ctx.moveTo(coord.x, coord.y);
	//now update the line coordinate to the new position from the event
	reposition(event);
	//set the lineTo to the new coordinate from the event
	ctx.lineTo(coord.x, coord.y);
	//draw the line
	ctx.stroke();
}

function eraseLastLine(event) {
	ctx.strokeStyle = 'white';
	ctx.lineSize = lineSize + 5;
	// ctx.lineTo(coord.x, coord.y);
	ctx.stroke();
}

function KeyPress(e) {
	var evtobj = e;
	e.preventDefault();
	// console.log(e);
	if (evtobj.keyCode == 90 && evtobj.ctrlKey) {
		redoBuffer.push(undoBuffer.pop());
		redrawAll();
	}
	if (evtobj.keyCode == 89 && evtobj.ctrlKey) {
		if (redoBuffer.length > 0) {
			undoBuffer.push(redoBuffer.pop());
			redrawAll();
		}
	}
}
