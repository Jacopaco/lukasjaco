/******/ (() => {
	// webpackBootstrap
	/******/ var __webpack_modules__ = {
		/***/ /*!******************************!*\
  !*** ./resources/js/site.js ***!
  \******************************/
		/***/ './resources/js/site.js': () => {
			//   This is all you :)
			const canvas = document.getElementById('canvas');
			const ctx = canvas.getContext('2d');

			//generate color palette buttons
			let colorPalette = [
				'#ffffffff',
				'#412234ff',
				'#6d466bff',
				'#b49fccff',
				'#ead7d7ff',
				'#e6c229ff',
				'#f17105ff',
				'#1a8fe3ff',
				'#397367ff',
				'#5da399ff'
			];
			colorPalette.forEach((color) => {
				let btn = document.createElement('button');

				btn.className = 'colorPaletteColorButton';
				btn.style.backgroundColor = color;
				btn.addEventListener('click', (event) => changeColor(color));
				document.getElementById('colorPalette').appendChild(btn);
			});

			var sizeChangeButtons = document.getElementsByClassName('changeSizeButton');
			Array.from(sizeChangeButtons, (button) => {
				button.addEventListener('click', (event) => changeSizeButton(event.target.value));
			});

			var sizeChangeInput = document.getElementById('lineSize');
			sizeChangeInput.addEventListener('input', (event) => changeSize(event.target.value));

			var clearAllButton = document.getElementById('clearAll');
			clearAllButton.addEventListener('click', (event) => clearAll());

			canvas.addEventListener('mousedown', start);
			canvas.addEventListener('mouseup', stop); //
			window.addEventListener('resize', resize);
			document.onkeydown = KeyPress;

			//global drawing variables
			let coord = { x: 0, y: 0 };

			var undoBuffer = [];
			var redoBuffer = [];
			//keeps line while it's being draw. On mouse release this gets pushed to drawing history.
			var currentLine = [];

			//init actions
			resize();
			initDrawingSettings();

			function initDrawingSettings() {
				//set the line properties
				ctx.lineWidth = 15;
				ctx.lineCap = 'round';
				ctx.lineJoin = 'round';
				//set initial value for linesize Input
				document.getElementById('lineSize').value = ctx.lineWidth;
			}

			function changeColor(newColor) {
				ctx.strokeStyle = newColor;
			}
			function changeSize(newSize) {
				ctx.lineWidth = newSize;
				document.getElementById('lineSize').value = ctx.lineWidth;
			}

			function changeSizeButton(type) {
				newSize = ctx.lineWidth;
				if (type === 'larger') {
					newSize += 5;
				}
				if (type === 'smaller') {
					if (newSize - 5 < 1) newSize = 1;
					else newSize -= 5;
				}
				changeSize(newSize);
			}

			function redrawAll() {
				console.log('redraw all');
				clearAll();

				undoBuffer.forEach((line) => {
					line.forEach((section) => {
						if (section.mode === 'start') {
							changeSize(section.size);
							changeColor(section.color);
							ctx.moveTo(section.x, section.y);
							ctx.beginPath();
							reposition({ clientX: section.x, clientY: section.y });
						}
						draw({ clientX: section.x, clientY: section.y });
					});
				});
			}

			function clearAll() {
				ctx.clearRect(0, 0, canvas.width, canvas.height);
			}

			//size the canvas to the size of the containing element -- should be something like parent element?
			function resize() {
				ctx.canvas.width = window.innerWidth;
				ctx.canvas.height = window.innerHeight;
			}

			//the mouse coordinates are relative to the entire screen, so the pixel coordinates need to be mapped to the canvas start location to
			//draw on the right spot
			function reposition(event) {
				coord.x = event.clientX - canvas.offsetLeft;
				coord.y = event.clientY - canvas.offsetTop;
			}

			//add event that draws on every mouse movement
			function start(event) {
				if (event.button != 0) return;
				redoBuffer = [];

				//move canvas "cursor" to place where drawing should start. endpoint is set in the draw event.
				ctx.moveTo(event.clientX, event.clientY);
				//begin path creates a new distinct line which seperates it from previously drawn lines, making new settings apply to only newly created lines.
				//leaving this out makes it so that if you change the color for a line, all previously drawn lines will change color as well.
				ctx.beginPath();
				draw(event);

				currentLine.push({
					x: event.clientX,
					y: event.clientY,
					size: ctx.lineWidth,
					color: ctx.strokeStyle,
					mode: 'start'
				});

				document.addEventListener('mousemove', mouseMoveEvent);
			}

			//remove event when button is released
			function stop(event) {
				if (event.button != 0) return;
				undoBuffer.push(currentLine);
				currentLine = [];
				document.removeEventListener('mousemove', mouseMoveEvent);
			}

			function mouseMoveEvent(event) {
				currentLine.push({
					x: event.clientX,
					y: event.clientY,
					size: ctx.lineSize,
					color: ctx.strokeStyle,
					mode: 'draw'
				});
				draw(event);
			}

			//draw the line
			function draw(event) {
				//translate mouseEvent coordinates to canvas
				reposition(event);
				ctx.lineCap = 'round';
				//set the lineTo to the new coordinate from the event
				ctx.lineTo(coord.x, coord.y);
				ctx.stroke();
			}

			function KeyPress(e) {
				var evtobj = e;
				// console.log(e);
				if (evtobj.keyCode == 90 && evtobj.ctrlKey) {
					if (undoBuffer.length == 0) return;
					redoBuffer.push(undoBuffer.pop());
					redrawAll();
				}
				if (evtobj.keyCode == 82 && evtobj.ctrlKey) {
					e.preventDefault();
					if (redoBuffer.length == 0) return;
					undoBuffer.push(redoBuffer.pop());
					redrawAll();
				}
			}

			/***/
		} /*!************************************!*\
  !*** ./resources/css/tailwind.css ***!
  \************************************/,

		/***/ /***/ './resources/css/tailwind.css': (
			__unused_webpack_module,
			__webpack_exports__,
			__webpack_require__
		) => {
			'use strict';
			__webpack_require__.r(__webpack_exports__);
			// extracted by mini-css-extract-plugin

			/***/
		}

		/******/
	}; // The module cache
	/************************************************************************/
	/******/ /******/ var __webpack_module_cache__ = {}; // The require function
	/******/

	/******/ /******/ function __webpack_require__(moduleId) {
		/******/ // Check if module is in cache
		/******/ var cachedModule = __webpack_module_cache__[moduleId];
		/******/ if (cachedModule !== undefined) {
			/******/ return cachedModule.exports;
			/******/
		} // Create a new module (and put it into the cache)
		/******/ /******/ var module = (__webpack_module_cache__[moduleId] = {
			/******/ // no module.id needed
			/******/ // no module.loaded needed
			/******/ exports: {}
			/******/
		}); // Execute the module function
		/******/

		/******/ /******/ __webpack_modules__[moduleId](module, module.exports, __webpack_require__); // Return the exports of the module
		/******/

		/******/ /******/ return module.exports;
		/******/
	} // expose the modules object (__webpack_modules__)
	/******/

	/******/ /******/ __webpack_require__.m = __webpack_modules__; /* webpack/runtime/chunk loaded */
	/******/

	/************************************************************************/
	/******/ /******/ (() => {
		/******/ var deferred = [];
		/******/ __webpack_require__.O = (result, chunkIds, fn, priority) => {
			/******/ if (chunkIds) {
				/******/ priority = priority || 0;
				/******/ for (var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--)
					deferred[i] = deferred[i - 1];
				/******/ deferred[i] = [ chunkIds, fn, priority ];
				/******/ return;
				/******/
			}
			/******/ var notFulfilled = Infinity;
			/******/ for (var i = 0; i < deferred.length; i++) {
				/******/ var [ chunkIds, fn, priority ] = deferred[i];
				/******/ var fulfilled = true;
				/******/ for (var j = 0; j < chunkIds.length; j++) {
					/******/ if (
						(priority & (1 === 0) || notFulfilled >= priority) &&
						Object.keys(__webpack_require__.O).every((key) => __webpack_require__.O[key](chunkIds[j]))
					) {
						/******/ chunkIds.splice(j--, 1);
						/******/
					} else {
						/******/ fulfilled = false;
						/******/ if (priority < notFulfilled) notFulfilled = priority;
						/******/
					}
					/******/
				}
				/******/ if (fulfilled) {
					/******/ deferred.splice(i--, 1);
					/******/ var r = fn();
					/******/ if (r !== undefined) result = r;
					/******/
				}
				/******/
			}
			/******/ return result;
			/******/
		};
		/******/
	})(); /* webpack/runtime/hasOwnProperty shorthand */
	/******/

	/******/ /******/ (() => {
		/******/ __webpack_require__.o = (obj, prop) => Object.prototype.hasOwnProperty.call(obj, prop);
		/******/
	})(); /* webpack/runtime/make namespace object */
	/******/

	/******/ /******/ (() => {
		/******/ // define __esModule on exports
		/******/ __webpack_require__.r = (exports) => {
			/******/ if (typeof Symbol !== 'undefined' && Symbol.toStringTag) {
				/******/ Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
				/******/
			}
			/******/ Object.defineProperty(exports, '__esModule', { value: true });
			/******/
		};
		/******/
	})(); /* webpack/runtime/jsonp chunk loading */
	/******/

	/******/ /******/ (() => {
		/******/ // no baseURI
		/******/

		/******/ // object to store loaded and loading chunks
		/******/ // undefined = chunk not loaded, null = chunk preloaded/prefetched
		/******/ // [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
		/******/ var installedChunks = {
			/******/ '/js/site': 0,
			/******/ 'css/tailwind': 0
			/******/
		}; /******/ /******/ /******/ /******/ /******/ // no chunk on demand loading // no prefetching // no preloaded // no HMR // no HMR manifest
		/******/

		/******/ /******/ /******/ /******/ /******/ /******/ __webpack_require__.O.j = (chunkId) =>
			installedChunks[chunkId] === 0; // install a JSONP callback for chunk loading
		/******/

		/******/ /******/ var webpackJsonpCallback = (parentChunkLoadingFunction, data) => {
			/******/ var [ chunkIds, moreModules, runtime ] = data; // add "moreModules" to the modules object, // then flag all "chunkIds" as loaded and fire callback
			/******/ /******/ /******/ var moduleId,
				chunkId,
				i = 0;
			/******/ if (chunkIds.some((id) => installedChunks[id] !== 0)) {
				/******/ for (moduleId in moreModules) {
					/******/ if (__webpack_require__.o(moreModules, moduleId)) {
						/******/ __webpack_require__.m[moduleId] = moreModules[moduleId];
						/******/
					}
					/******/
				}
				/******/ if (runtime) var result = runtime(__webpack_require__);
				/******/
			}
			/******/ if (parentChunkLoadingFunction) parentChunkLoadingFunction(data);
			/******/ for (; i < chunkIds.length; i++) {
				/******/ chunkId = chunkIds[i];
				/******/ if (__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
					/******/ installedChunks[chunkId][0]();
					/******/
				}
				/******/ installedChunks[chunkId] = 0;
				/******/
			}
			/******/ return __webpack_require__.O(result);
			/******/
		};
		/******/

		/******/ var chunkLoadingGlobal = (self['webpackChunk'] = self['webpackChunk'] || []);
		/******/ chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
		/******/ chunkLoadingGlobal.push = webpackJsonpCallback.bind(
			null,
			chunkLoadingGlobal.push.bind(chunkLoadingGlobal)
		);
		/******/
	})(); // startup // Load entry module and return exports // This entry module depends on other loaded chunks and execution need to be delayed
	/******/

	/************************************************************************/
	/******/

	/******/ /******/ /******/ /******/ __webpack_require__.O(undefined, [ 'css/tailwind' ], () =>
		__webpack_require__('./resources/js/site.js')
	);
	/******/ var __webpack_exports__ = __webpack_require__.O(undefined, [ 'css/tailwind' ], () =>
		__webpack_require__('./resources/css/tailwind.css')
	);
	/******/ __webpack_exports__ = __webpack_require__.O(__webpack_exports__);
	/******/
	/******/
})();
